import {LOGIN_SUCCESSS, LOGIN_FAIL} from '../constants';
import {createReducer} from '../utils';

const initialState = {
  isFetiching: false,
  loginSuccess: '',
  loginFailed: '',
};

export default createReducer(initialState, {
  [LOGIN_SUCCESSS]: state =>
    Object.assign({}, state, {
      isFetiching: false,
      loginSuccess: 'success',
      loginFailed: '',
    }),
  [LOGIN_FAIL]: state =>
    Object.assign({}, state, {
      isFetiching: false,
      loginSuccess: '',
      loginFailed: 'Something went wrong,Please try again later ',
    }),
});
